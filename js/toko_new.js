if (document.readyState == 'loading') {
    document.addEventListener('DOMContentLoaded', ready)
} else {
    ready()
}

function ready() {
    $('.parallax').parallax()
    $('.tabs').tabs({
      swipeable : true,
      responsiveThreshold : 1920
    })
    $('.modal').modal()
    // $('.tabs-content.carousel.carousel-slider').css("height","auto")

    var addToCartButtons = document.getElementsByClassName('shop-item-button')
    for (var i = 0; i < addToCartButtons.length; i++) {
        var button = addToCartButtons[i]
        button.addEventListener('click', addToCartClicked)
    }

    var shopItemImages = document.getElementsByClassName('shop-item-image')
    for (var i = 0; i < shopItemImages.length; i++) {
        var itemImage = shopItemImages[i]
        itemImage.addEventListener('click', clickDetail)
    }

    $("#btn-cart").click(function() {
        $('html,body').animate({
            scrollTop: $("#order-section").offset().top},
            'slow')
    })
}

function clickDetail(event) {
    var modal = M.Modal.getInstance(modaldetail)
    var itemImage = event.target.parentElement.getElementsByClassName('shop-item-image')[0].src
    var itemNote = event.target.parentElement.getElementsByClassName('shop-item-note')[0].innerText
    document.getElementsByClassName('detail-image')[0].src = itemImage
    document.getElementsByClassName('detail-note')[0].innerText = itemNote
    modal.open()
}

function quantityAdd(event) {
    var input = event.target.parentElement.parentElement
    var inputCount = input.getElementsByClassName('cart-item-count')[0]
    var price = input.getElementsByClassName('cart-item-price')[0].innerText
    
    var count = parseInt(inputCount.innerText) + 1
    inputCount.innerHTML = count
    updateCartTotal(price)
}

function quantityReduce(event) {
    var input = event.target.parentElement.parentElement
    var inputCount = input.getElementsByClassName('cart-item-count')[0]
    var price = input.getElementsByClassName('cart-item-price')[0].innerText
    
    var count = parseInt(inputCount.innerText) - 1
    if(count == 0) {
        input.remove()
    }
    inputCount.innerHTML = count
    updateCartTotal(price,'-')
}

function addToCartClicked(event) {
    var button = event.target
    var shopItem = button.parentElement.parentElement
    var title = shopItem.getElementsByClassName('shop-item-title')[0].innerText
    var price = shopItem.getElementsByClassName('shop-item-price')[0].innerText
    var imageSrc = shopItem.getElementsByClassName('shop-item-image')[0].src
    addItemToCart(title, price, imageSrc)
    updateCartTotal(price)
    M.toast({html: title+' ditambahkan'})
}

function addItemToCart(title, price, imageSrc) {
    var cartRow = document.createElement('tr')
    cartRow.classList.add('cart-row')
    var cartItems = document.getElementsByClassName('cart-items')[0]
    var cartItemNames = cartItems.getElementsByClassName('cart-item-title')
    var cartItemCount = cartItems.getElementsByClassName('cart-item-count')
    
    for (var i = 0; i < cartItemNames.length; i++) {
        if (cartItemNames[i].innerText == title) {
            var count = parseInt(cartItemCount[i].innerText) + 1
            cartItemCount[i].innerHTML = count
            return
        }
    }

    var cartRowContents = `
                <tr>
                  <td><i class="material-icons cart-quantity-reduce" style="cursor: pointer;">indeterminate_check_box</i></td>
                  <td><span class="cart-item-title">${title}</span> (<span class="cart-item-count">1</span>)</td>
                  <td style="display:none"><span class="cart-item-price">${price}</span></td>
                  <td><i class="material-icons cart-quantity-add" style="cursor: pointer;">add_box</i></td>
                </tr>`

    cartRow.innerHTML = cartRowContents
    cartItems.append(cartRow)

    cartRow.getElementsByClassName('cart-quantity-add')[0].addEventListener('click', quantityAdd)
    cartRow.getElementsByClassName('cart-quantity-reduce')[0].addEventListener('click', quantityReduce)
}

function updateCartTotal(price,operation='+') {
    var cartTotalCount = document.getElementsByClassName('cart-total-count')[0]
    var cartTotalPrice = document.getElementsByClassName('cart-total-price')[0]
    if (operation == '+') {
        var totalCount = parseInt(cartTotalCount.innerText) + 1
        var totalPrice = parseInt(cartTotalPrice.innerText.split('.').join('')) + parseInt(price.split('.').join(''))
    } else {
        var totalCount = parseInt(cartTotalCount.innerText) - 1
        var totalPrice = parseInt(cartTotalPrice.innerText.split('.').join('')) - parseInt(price.split('.').join(''))
    }

    if (totalCount == 0) {
        document.getElementById("btn-finish-order").disabled = true
    } else {
        document.getElementById("btn-finish-order").disabled = false
    }

    cartTotalCount.innerHTML = totalCount
    cartTotalPrice.innerHTML = totalPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    document.getElementsByClassName('cart-total')[0].innerHTML = `<strong>Rp `+totalPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')+`</strong> (`+totalCount+`)`

}

$('#form-order').submit(function (evt) {
    console.log('s')
    evt.preventDefault()
    window.history.back()
})

window.onscroll = function(ev) {
    if ((window.innerHeight + window.pageYOffset + 200) >= (document.body.offsetHeight)) {
        document.getElementsByClassName('fixed-action-btn')[0].classList.add("anim-hide")
        document.getElementsByClassName('fixed-action-btn')[0].classList.remove("anim-show")
    } else {
        document.getElementsByClassName('fixed-action-btn')[0].classList.add("anim-show")
    }
}